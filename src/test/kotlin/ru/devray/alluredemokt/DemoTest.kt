package ru.devray.alluredemokt

import org.testng.Assert
import org.testng.annotations.DataProvider
import org.testng.annotations.Ignore
import org.testng.annotations.Test
import kotlin.random.Random

class DemoTest {
    @Test
    fun testSomethingThatIsOk() {
        Assert.assertTrue(true)
    }

    @Test
    fun testSomethingThatIsAlsoOk() {
        Assert.assertTrue(true)
    }

    @Test
    fun testSomethingThatFails() {
        Assert.assertTrue(false)
    }

    @Test
    fun testFlakyAndUnpredictableOne() {
        Assert.assertTrue(Random.nextBoolean())
    }

    @Test
    @Ignore
    fun testThatWePreferToIgnore() {
        Assert.assertTrue(Random.nextBoolean())
    }

    @Test
    fun testThatIsBroken() {
        throw RuntimeException()
    }

    @Test(dataProvider = "randomNumbers")
    fun testsThatAreParameterized(number: Int) {
        Assert.assertTrue(number >= 5)
    }

    @Test
    fun testFlakyAndUnpredictableTwo() {
        Assert.assertTrue(Random.nextBoolean())
    }

    @DataProvider
    fun randomNumbers(): Array<Int> = Array(Random.nextInt(7, 10)) { Random.nextInt(10) }
}