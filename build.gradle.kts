import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.5.10"
    id("io.qameta.allure") version "2.9.6"
}

group = "ru.devray"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(kotlin("test"))

    testImplementation("io.qameta.allure:allure-testng:2.17.1")
    testImplementation("io.qameta.allure:allure-attachments:2.17.1")
    testImplementation("io.qameta.allure:allure-generator:2.17.1")
    testImplementation("io.qameta.allure:allure-java-commons:2.17.1")
}

allure {
    version.set("2.17.1")
    report {
        reportDir.set(file("$projectDir/build/allure-report"))
    }
}

tasks.test {
    useTestNG()
}

tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "11"
}