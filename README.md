# Пример настройки истории Allure для проекта Kotlin + Gradle + TestNG
## Описание процесса формирования истории
Тренды (история) прежних тестовых прогонов - крайне важная метрика для оценки качества и стабильности как разработанных фич, так и разработанных автотестов.
Пример отчета с трендами можно найти [тут](https://demo.qameta.io/allure/).
Ниже мы рассмотрим минимальные необходимые шаги CI пайплайна для настройки отчетности в GitLab CI для демо-проекта Kotlin + Gradle + TestNG.
### 1. Прогон тестов и генерация результатов (allure-results)
В результате выполнения команды <code>gradle clean test</code> запускаем наши тесты, получаем сгенерированные результаты прогона тестов.
В первую очередь нас интересуют аллюр результаты в директории **build/allure-results**
### 2. Обогащение результатов - добавляем историю прошлых запусков
Но одних только результатов текущего прогона недостаточно, нужна еще щепотка истории для красивых репортов.
История берется из артефактов шага **pages** и выгружается на данном этапе в качестве архива (через запрос к API GitLab к артефактам предыдущего результата джобы pages)
В архиве нас интересует единственный блок данных - папка **history**, в которой хранятся данные о прежних тестовых запусках.
Копируем эту директорию в allure-results.
<code>**allure-resuts + history = основа для построения отчета с трендами**</code>
### 3. Генерируем репорт с историчностью (трендами)
Это самый простой и прозрачный шаг. Мы просто генерируем отчет на базе собранных в allure-results данных.
Из этого шага мы экспортируем полученную allure-reports директорию для хоста на GitLab Pages.
### 4. Деплоим (хостим) allure-репорт на GitLab pages
Шаг посвящен собственно деплою нашего репорта на компактный сервачок GitLab Pages.
Доступ к отчету можно получить через UI гитлаба:
Левое меню - Settings - Pages - раздел Access pages - там будет представлен URL/URLы всех хостящихся в Pages страничек.
### Наглядная схема всего процесса
![Схема процесса в CI](ci-process.png)
_..почему я не нарисовал это все в draw.io - вопрос хороший. но ответа на него нет._
